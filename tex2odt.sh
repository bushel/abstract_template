pandoc $1 -f latex -t odt -o output.odt --bibliography iturriak1.bib --filter pandoc-citeproc --csl global/bst/apa.csl -M reference-section-title=References -M link-citations=False --number-sections

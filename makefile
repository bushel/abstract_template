# modified version of original from:
# https://github.com/langsci/latex-skeletons

# specify the main file and all the files that you are including
MAINFILE=main
SOURCE=$(MAINFILE).tex $(wildcard local*.tex) $(wildcard chapters/*.tex) 

bbl:  $(SOURCE) iturriak1.bib  
	xelatex -no-pdf $(MAINFILE) 
	biber   $(MAINFILE) 

pdf: $(MAINFILE).aux
	xelatex $(MAINFILE) 

#housekeeping	
clean:
	rm -f *.bak *~ *.backup *.tmp \
	*.adx *.and *.idx *.ind *.ldx *.lnd *.sdx *.snd *.rdx *.rnd *.wdx *.wnd \
	*.log *.blg *.ilg \
	*.aux *.toc *.cut *.out *.tpm *.bbl *-blx.bib *_tmp.bib *bcf \
	*.glg *.glo *.gls *.wrd *.wdv *.xdv *.mw *.clr \
	*.run.xml \

